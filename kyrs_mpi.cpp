#include "mpi.h" 
#include <stdio.h>
#include <math.h>  
#include <stdlib.h>
#include <fstream>

#define pi 3.14

using namespace std;
int main ( int  argc, char **argv) 
{
int size,myrank;
 MPI_Status status;
 MPI_Init (&argc, &argv); 
 MPI_Comm_rank (MPI_COMM_WORLD, &myrank);
 MPI_Comm_size (MPI_COMM_WORLD, &size);
	int N,root=0,step,count=0,MaxIter=2000;
	float eps, h,t1,t2,t3,t4,d,max,norm;
	N = 50;
	h = 1.0 / N;
	eps = 0.001*h*h;
	step=(N-2)/size;
	float** f = new float*[step];
	//
	//// Каждый процесс вычисляет свою часть матрицы f
	//
	for (int i = 0 ; i< step;i++)
			{
				f[i]=new float[N];
				for (int j=0 ; j< N;j++)
				{	
					f[i][j]=2 * pi * pi * sin(h * (i+step*myrank) * pi) * sin(h * j * pi);
					//f_recv[i*step+j] = 2 * pi * pi * sin(h * (i+step*myrank) * pi) * sin(h * j * pi);
					//cout << "f[" << i+step*myrank << "][" << j << "]= " << f[i][j] << endl;
				}
			}	
	float**u = new float*[step];//у каждого процессора свой массив 
	float*u_buf = new float[step*N];//у каждого процессора свой массив
	float*top = new float[N];//массивы для верхней части блока
	float*btn = new float[N];//массивы для нижней части блока 
	float*u_recv = new float[N*N];
	float**u_res;
	//
	//Выделяем память под результирующий массив
	//
	if (myrank==root)
	{
		
		u_res = new float*[N];
		for (int i = 0 ; i<N;i++)
		{
			u_res[i]=new float[N];
		}
	}
	//Инициирем значения массивов на каждом процессе
	for (int i = 0 ; i < step ; i++)
	{
		u[i]=new float[N];
		for (int j = 0; j < N; j++)
				{
					u[i][j] = 0;
					top[j]=btn[j]=0;
				}
	}
	//cout<<"Start calculation\n";
	float total_send=0;
	t1=MPI_Wtime();
	//каждый процесс начинает вычисления 
		do
		{
			count++;
			t3=MPI_Wtime();
			//передаем значения верхних и нижних строк для кадого блока 
			if (myrank==0)
					{
						int nextProc=myrank+1;
						//cout<<" Proc "<<myrank<<" start send recive to next proc "<<nextProc<<endl;
						MPI_Sendrecv(u[step-1], N, MPI_REAL, nextProc, 99, btn, N, MPI_REAL, nextProc, 99, MPI_COMM_WORLD, &status );
					}
			else if (myrank==size-1)
					{
						int prevProc=myrank-1;
						//cout<<" Proc "<<myrank<<" start send recive to prev proc "<<prevProc<<endl;
						MPI_Sendrecv( u[0], N, MPI_REAL, prevProc, 99, top, N, MPI_REAL, prevProc, 99, MPI_COMM_WORLD, &status );
					}
			else 
			{ 
						int nextProc=myrank+1;
						int prevProc=myrank-1;
						//cout<<" Proc "<<myrank<<" start send recive to next proc "<<nextProc<<" and prev proc "<<prevProc<<endl;
						MPI_Sendrecv( u[step-1], N, MPI_REAL, nextProc, 99, btn, N, MPI_REAL, nextProc, 99, MPI_COMM_WORLD, &status );
						MPI_Sendrecv( u[0], N, MPI_REAL, prevProc, 99, top, N, MPI_REAL, prevProc, 99, MPI_COMM_WORLD, &status);
			}
			t4=MPI_Wtime();
			total_send+=t4-t3;
			//cout<<"Proc "<<myrank<<" start calculation\n";
			//после того ка все значения переданы/получены ачинаем считать 
			max=0;
			for (int i = 0; i < step; i++)
					{
						for (int j = 1; j < N - 1; j++)
						{
							float u0 = u[i][j];
							if (i==0)
							{
								u[i][j] = 0.25 * (top[j] + u[i + 1][j] + u[i][j - 1] + u[i][j + 1] + h * h * f[i][j]);
							}
							else if (i==step-1)
							{
								u[i][j] = 0.25 * (u[i - 1][j] + btn[j] + u[i][j - 1] + u[i][j + 1] + h * h * f[i][j]);
							}
							else
							{
								u[i][j] = 0.25 * (u[i - 1][j] + u[i + 1][j] +u[i][j - 1] + u[i][j + 1] + h * h * f[i][j]);
							}
							d = abs(u[i][j] - u0);
							if (d > max) max = d;
						}
					}	
			//cout<<"Max dif on "<<myrank<<" proc is "<<max<<endl;
			t3=MPI_Wtime();
			//ждем пока все посчитают чтобы найти максимум
			MPI_Barrier(MPI_COMM_WORLD);
			//находим максимальную норму 
			MPI_Reduce(&max, &norm, 1,MPI_REAL,MPI_MAX,root,MPI_COMM_WORLD);
			//расылаем максимальную норму все процессам
			MPI_Bcast(&norm,1,MPI_REAL,root,MPI_COMM_WORLD); 	
			t4=MPI_Wtime();
			total_send+=t4-t3;			
		}while ((norm > eps)&&(count<MaxIter));
		t2=MPI_Wtime();
		if (myrank==root)
		{
			cout<<"Time: "<<t2-t1<<" Iter: "<<count<<" Send time: "<<total_send<<endl;
		}
		
		//готовим массивы к отправке 
		for (int i = 0 ; i<step ; i++)
		{
			for (int j = 0 ; j< N;j++)
			{
				u_buf[i*step+j]=u[i][j];
				//cout<<u_buf[i*step+j]<<endl;
			}
		}
		MPI_Barrier(MPI_COMM_WORLD);
		//собираем массивы в общии мсив 
		MPI_Gather(u_buf,step*N,MPI_REAL,u_recv,step*N,MPI_REAL,root,MPI_COMM_WORLD);
		if (myrank==root)
		{
			cout<<"After gather\n";
			for (int i = 0 ; i< N;i++)
			{
				for (int j = 0 ; j< N;j++)
				{
					u_res[i][j]=u_recv[i*N+j];
				//	cout<<u_recv[i*N+j]<<endl;
				}
			}
			max=0;
				for (int i = 0 ; i<N;i++)
				{
					for (int j=0 ; j< N;j++)
					{
						double d=abs(u_res[i][j] - sin(pi*h*i)*sin(pi*h*j));
						if (d>max) max=d;
					}
				}
	cout<<max<<endl;
			//записываем результат в файл
			ofstream myfile;
			myfile.open("result_mpi.txt",ios::out);
			for (int i = 0 ; i< N;i++)
			{
				for (int j=0 ; j< N;j++)
				{
					myfile << u_res[i][j] << " ";
				}
				myfile<<endl;
			}
		}
MPI_Finalize(); 
return 0;
}