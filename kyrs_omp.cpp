#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>
#include <cstdlib>
#include <omp.h>

using namespace std;
#define pi 3.14


int main(int argc, char** argv)
{
	double** u, ** f;
	int N, IterCnt = 0,MaxIter=1000;
	double eps, h;
	N = 50;
	eps = 0.001*h*h;
	h = 1.0 / (N );
	f = new double* [N];
	//инициализируем значения матрицы f
	for (int i = 0; i < N; i++)
	{
		f[i] = new double[N];
		for (int j = 0; j < N; j++)
			f[i][j] = 2 * pi * pi * sin(h * i * pi) * sin(h * j * pi);
	}
	u = new double* [N];
	//инициализируем значения матрицы u
	for (int i = 0; i < N; i++)
	{
		u[i] = new double[N];
		for (int j = 0; j < N; j++)
		{
			u[i][j] = 0;
		}
	}
	double tt = omp_get_wtime();
	double max;
IterCnt = 0;
//начинаем считать 
	do
	{
		IterCnt++;
		max = 0;
		//распаралеливанем по внешнему цциклу с общим доступом к матрице u
		#pragma omp parallel for shared(u,N,max) 
			for (int i = 1; i < N-1; i++)
			{
			double max0 = 0;
			for (int j = 1; j < N-1; j++)
			{
				double u0 = u[i][j];
				u[i][j] = 0.25*(u[i-1][j] + u[i+1][j]+ u[i][j-1] + u[i][j+1] + h*h*f[i][j]);
				double d = abs(u[i][j]-u0);
				if (d > max0)
				max0 = d;
			}
			//если вычисленная норма больше чем глобальная, к которой есть доступ, то перезаписываем ее в critical
			if (max0 > max)
			#pragma omp critical
			if (max0 > max)max = max0;
			}

	}
 while ((max > eps)&&(IterCnt<MaxIter));
	tt = omp_get_wtime() - tt;
	cout << "Time = " << tt << " IterCnt = " << IterCnt << endl;
	max=0;
	//считаем максимальное отклонение от точного решения 
	for (int i = 0 ; i<N;i++)
	{
		for (int j=0 ; j< N;j++)
		{
			double d=abs(u[i][j] - sin(pi*h*i)*sin(pi*h*j));
			if (d>max) max=d;
		}
	}
	cout<<max<<endl;
	//записываем результата в файл.
	ofstream myfile;
	myfile.open("result1.txt",ios::out);
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			myfile << u[i][j] << " ";
		}
		myfile << endl;
	}
	return 0;
}
